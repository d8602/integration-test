package integrationtest.dynamodb;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.services.lambda.LambdaClient;
import software.amazon.awssdk.services.lambda.model.Runtime;
import software.amazon.awssdk.services.lambda.model.*;
import software.amazon.awssdk.services.lambda.waiters.LambdaWaiter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PurgeDynamoDBTest {

    private static final String ENDPOINT_URL = "http://localhost:4566";
    private static final String REGION = "eu-central-1";
    private static final AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(ENDPOINT_URL, REGION);
    private static final AmazonDynamoDB ddbClient = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(endpointConfiguration).build();
    private static final DynamoDB ddb = new DynamoDB(ddbClient);
    private static final LambdaClient lambda = LambdaClient.builder().endpointOverride(URI.create(ENDPOINT_URL)).build();

    private static final String TABLE_NAME = "TEST_TABLE";
    private static final String FUNCTION_NAME = "DYNAMO_PURGE_FUNCTION";


    @BeforeAll
    static void createInfrastructure() throws FileNotFoundException {
        deleteTable();
        createTable();
        deleteLambdaFunction();
        createLambdaFunction();
    }

    @AfterAll
    static void deleteInfrastructure() {
        deleteTable();
        deleteLambdaFunction();
    }

    @Test
    void test_PurgeDynamoDB_Handler() throws JsonProcessingException {

        var NUM_OF_ITEMS = 999;

        loadTable(NUM_OF_ITEMS);

        PurgeRequest purgeRequest = new PurgeRequest();
        purgeRequest.setTableName(TABLE_NAME);

        ObjectMapper objectMapper = new ObjectMapper();
        SdkBytes payload = SdkBytes.fromUtf8String(objectMapper.writeValueAsString(purgeRequest));

        InvokeRequest invokeRequest = InvokeRequest.builder().functionName(FUNCTION_NAME).payload(payload).build();
        InvokeResponse invokeResponse = lambda.invoke(invokeRequest);
        System.out.println(invokeResponse.payload().asUtf8String());

        var expectedResponse = new PurgeResponse();
        expectedResponse.setNumberOfTableEntries(NUM_OF_ITEMS);

        assertLambdaResponse(expectedResponse, objectMapper.readValue(invokeResponse.payload().asUtf8String(), PurgeResponse.class));
        assertDynamoTable();
    }


    // region -------- test helper functions ----------

    static void assertLambdaResponse(PurgeResponse expectedResponse, PurgeResponse actualResponse) {

        assertEquals(expectedResponse.getNumberOfTableEntries(), actualResponse.getNumberOfTableEntries());

    }

    static void assertDynamoTable() {

        String keyNames = getKeyNames(TABLE_NAME);
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TABLE_NAME)
                .withReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL)
                .withProjectionExpression(keyNames);

        ScanResult scanResult = ddbClient.scan(scanRequest);

        assertEquals(0, scanResult.getScannedCount());

    }

    static String getKeyNames(String tableName) {

        String keyNames = null;

        TableCollection<ListTablesResult> tableCollection = ddb.listTables();
        for (Table table : tableCollection) {
            if (table.getTableName().equals(tableName)) {
                List<KeySchemaElement> keys = table.describe().getKeySchema();
                keyNames = keys.get(0).getAttributeName() + ", " + keys.get(1).getAttributeName();
            }
        }

        return keyNames;
    }

    // endregion


    // region -------- test setup utility methods -----------

    static void createTable() {
        CreateTableRequest createTableRequest = new CreateTableRequest()
                .withTableName(TABLE_NAME)
                .withAttributeDefinitions(
                        new AttributeDefinition("hKey", ScalarAttributeType.S),
                        new AttributeDefinition("sKey", ScalarAttributeType.S)
                )
                .withKeySchema(
                        new KeySchemaElement("hKey", KeyType.HASH),
                        new KeySchemaElement("sKey", KeyType.RANGE)
                )
                .withProvisionedThroughput(
                        new ProvisionedThroughput(20L, 20L)
                );

        ddb.createTable(createTableRequest);

    }

    static void deleteTable() {

        TableCollection<ListTablesResult> tables = ddb.listTables();
        tables.forEach(table -> {
            if (table.getTableName().equals(TABLE_NAME)) {
                table.delete();
                try {
                    table.waitForDelete();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

    }

    static void loadTable(int numberOfItems) {

        TableWriteItems batchItems;
        List<Item> items = new ArrayList<>();

        System.out.println("Writing " + numberOfItems + " items to " + TABLE_NAME + " ...");
        for (int i = 1; i <= numberOfItems; i++) {
            items.add(new Item().withPrimaryKey("hKey", String.valueOf(i), "sKey", String.valueOf(i)));
            if (i % 25 == 0 || i == numberOfItems) {
                batchItems = new TableWriteItems(TABLE_NAME).withItemsToPut(items);
                BatchWriteItemOutcome outcome = ddb.batchWriteItem(batchItems);
                if (outcome.getUnprocessedItems().size() > 0) {
                    System.out.println("Unprocessed items for this batch: " + outcome.getUnprocessedItems().size());
                }
                items = new ArrayList<>();
            }

        }
        System.out.println(TABLE_NAME + " has been initialized.");

    }

    static void createLambdaFunction() throws FileNotFoundException {

        InputStream is = new FileInputStream("../../out/artifacts/dynamodb_purge/dynamodb-purge.jar");
        SdkBytes jarFile = SdkBytes.fromInputStream(is);

        FunctionCode code = FunctionCode.builder()
                .zipFile(jarFile)
                .build();

        var envVars = Map.of("ENDPOINT_URL", ENDPOINT_URL, "REGION", REGION);
        Environment functionEnvironment = Environment.builder().variables(envVars).build();

        CreateFunctionRequest createFunctionRequest = CreateFunctionRequest.builder()
                .functionName(FUNCTION_NAME)
                .runtime(Runtime.JAVA11)
                .code(code)
                .handler("integrationtest.dynamodb.PurgeDynamoDB")
                .role("dummy-role")
                .environment(functionEnvironment)
                .build();

        CreateFunctionResponse functionResponse = lambda.createFunction(createFunctionRequest);
        GetFunctionRequest getFunctionRequest = GetFunctionRequest.builder().functionName(FUNCTION_NAME).build();
        LambdaWaiter waiter = lambda.waiter();

        WaiterResponse<GetFunctionResponse> waiterResponse = waiter.waitUntilFunctionExists(getFunctionRequest);
        waiterResponse.matched().response().ifPresent(System.out::println);
        System.out.println("The function ARN is " + functionResponse.functionArn());

    }

    static void deleteLambdaFunction() {
        ListFunctionsResponse listFunctionsResponse = lambda.listFunctions();
        listFunctionsResponse.functions().forEach(function -> {
            if (function.functionName().equals(FUNCTION_NAME)) {
                DeleteFunctionRequest deleteFunctionRequest = DeleteFunctionRequest.builder().functionName(FUNCTION_NAME).build();
                lambda.deleteFunction(deleteFunctionRequest);
            }
        });
    }

    // endregion
}
package integrationtest.dynamodb;

import java.io.Serializable;

public class PurgeResponse implements Serializable {

    private Double consumedTotalCapacity = 0.0;

    private Double consumedReadCapacity = 0.0;

    private Double consumedWriteCapacity = 0.0;

    private Integer numberOfTableEntries = 0;

    private Integer numberOfUnprocessedItems = 0;


    private long purgeExecutionTime = 0L;


    public long getPurgeExecutionTime() {
        return purgeExecutionTime;
    }

    public void setPurgeExecutionTime(long purgeExecutionTime) {
        this.purgeExecutionTime = purgeExecutionTime;
    }

    public Integer getNumberOfUnprocessedItems() {
        return numberOfUnprocessedItems;
    }

    public void setNumberOfUnprocessedItems(Integer numberOfUnprocessedItems) {
        this.numberOfUnprocessedItems = numberOfUnprocessedItems;
    }

    public Double getConsumedTotalCapacity() {
        return consumedTotalCapacity;
    }

    public void setConsumedTotalCapacity(Double consumedTotalCapacity) {
        this.consumedTotalCapacity = consumedTotalCapacity;
    }

    public Double getConsumedReadCapacity() {
        return consumedReadCapacity;
    }

    public void setConsumedReadCapacity(Double consumedReadCapacity) {
        this.consumedReadCapacity = consumedReadCapacity;
    }

    public Double getConsumedWriteCapacity() {
        return consumedWriteCapacity;
    }

    public void setConsumedWriteCapacity(Double consumedWriteCapacity) {
        this.consumedWriteCapacity = consumedWriteCapacity;
    }

    public Integer getNumberOfTableEntries() {
        return numberOfTableEntries;
    }

    public void setNumberOfTableEntries(Integer numberOfTableEntries) {
        this.numberOfTableEntries = numberOfTableEntries;
    }

}

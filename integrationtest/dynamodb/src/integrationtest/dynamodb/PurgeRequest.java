package integrationtest.dynamodb;

import java.io.Serializable;

public class PurgeRequest implements Serializable {

    private String tableName;


    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
}

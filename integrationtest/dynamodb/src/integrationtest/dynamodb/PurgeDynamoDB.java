package integrationtest.dynamodb;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.BatchWriteItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class PurgeDynamoDB implements RequestHandler<PurgeRequest, PurgeResponse> {

    private DynamoDB ddb;

    @Override
    public PurgeResponse handleRequest(PurgeRequest purgeRequest, Context context) {

        final PurgeResponse response = new PurgeResponse();

        AmazonDynamoDB ddbClient;
        final String ENDPOINT_URL = System.getenv().get("ENDPOINT_URL");
        final String REGION = System.getenv().get("REGION");

        if (ENDPOINT_URL != null && REGION != null) {
            AwsClientBuilder.EndpointConfiguration endpointConfiguration = new AwsClientBuilder.EndpointConfiguration(ENDPOINT_URL, REGION);
            ddbClient = AmazonDynamoDBClientBuilder.standard()
                    .withEndpointConfiguration(endpointConfiguration)
                    .build();
        } else {
            ddbClient = AmazonDynamoDBClientBuilder.defaultClient();
        }

        ddb = new DynamoDB(ddbClient);

        Instant start = Instant.now();

        // scan table to retrieve keys
        String tableName = purgeRequest.getTableName();
        String keyNames = getKeyNames(tableName);
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(tableName)
                .withReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL)
                .withProjectionExpression(keyNames);

        ScanResult scanResult = ddbClient.scan(scanRequest);

        // build response object
        response.setNumberOfTableEntries(scanResult.getScannedCount());
        response.setConsumedTotalCapacity(scanResult.getConsumedCapacity().getCapacityUnits());
        response.setConsumedReadCapacity(scanResult.getConsumedCapacity().getCapacityUnits());

        // delete items in batches
        deleteBatchItems(tableName, getKeySchema(tableName), scanResult, response);

        Instant finish = Instant.now();
        response.setPurgeExecutionTime(Duration.between(start, finish).getSeconds());

        return response;
    }


    /*
        No retry has been implemented for unprocessed items.
     */
    private void deleteBatchItems(String tableName, List<KeySchemaElement> keySchemaElements, ScanResult scanResult, PurgeResponse purgeResponse) {

        String hashKeyName = keySchemaElements.get(0).getKeyType().equals(KeyType.HASH.toString()) ?
                keySchemaElements.get(0).getAttributeName() :
                keySchemaElements.get(1).getAttributeName();

        String rangeKeyName = keySchemaElements.get(0).getKeyType().equals(KeyType.HASH.toString()) ?
                keySchemaElements.get(1).getAttributeName() :
                keySchemaElements.get(0).getAttributeName();

        List<PrimaryKey> primaryKeys = new ArrayList<>();
        List<Map <String, AttributeValue>> items = scanResult.getItems();
        for (int i = 0; i < items.size(); i++) {

            var hashKeyValue = items.get(i).get(hashKeyName).getS();
            var rangeKeyValue = items.get(i).get(rangeKeyName).getS();
            System.out.println(hashKeyName + " - " + hashKeyValue + " - " + rangeKeyName + " - " + rangeKeyValue);
            primaryKeys.add(new PrimaryKey(hashKeyName, hashKeyValue, rangeKeyName, rangeKeyValue));

            if( primaryKeys.size() == 25 || i == items.size() -1 ) {
                TableWriteItems tableWriteItems = new TableWriteItems(tableName).withPrimaryKeysToDelete(primaryKeys.toArray(PrimaryKey[]::new));
                BatchWriteItemSpec batchWriteItemSpec = new BatchWriteItemSpec().withTableWriteItems(tableWriteItems).withReturnConsumedCapacity(ReturnConsumedCapacity.TOTAL);
                BatchWriteItemOutcome batchWriteItemOutcome = ddb.batchWriteItem(batchWriteItemSpec);
                if (batchWriteItemOutcome.getUnprocessedItems() != null) {
                    purgeResponse.setNumberOfUnprocessedItems(purgeResponse.getNumberOfUnprocessedItems() + batchWriteItemOutcome.getUnprocessedItems().size());
                }
                purgeResponse.setConsumedWriteCapacity(purgeResponse.getConsumedWriteCapacity() +
                        batchWriteItemOutcome.getBatchWriteItemResult().getConsumedCapacity().get(0).getCapacityUnits());
                purgeResponse.setConsumedTotalCapacity(purgeResponse.getConsumedTotalCapacity() +
                        batchWriteItemOutcome.getBatchWriteItemResult().getConsumedCapacity().get(0).getCapacityUnits());
                primaryKeys = new ArrayList<>();
            }
        }

    }


    /*
        returns a string with comma separated key names
     */
    private String getKeyNames(String tableName) {

        String keyNames = null;

        TableCollection<ListTablesResult> tableCollection = ddb.listTables();
        for (Table table : tableCollection) {
            if (table.getTableName().equals(tableName)) {
                List<KeySchemaElement> keys = table.describe().getKeySchema();
                keyNames = keys.get(0).getAttributeName() + ", " + keys.get(1).getAttributeName();
            }
        }

        return keyNames;
    }


    private List<KeySchemaElement> getKeySchema(String tableName) {

        TableCollection<ListTablesResult> tableCollection = ddb.listTables();
        for (Table table : tableCollection) {
            if (table.getTableName().equals(tableName)) {
                return  table.describe().getKeySchema();
            }
        }

        return new ArrayList<>();
    }

}
